﻿using System;

namespace Geo_Calculator
{
    class OneTypeExeption : Exception
    {
        byte Val1 { get; set; }
        byte Val2 { get; set; }
        public string Message1 { get; set; }

        public OneTypeExeption(string message, byte Val1, byte Val2)
        {
            this.Val1 = Val1;
            this.Val2 = Val2;
            Message1 = CreateMessage() + message + ". ";
        }

        private string CreateMessage()
        {
            string message = "";

            if (Val1 == Val2)
            {
                message += "same type selected (";
                if (Val1 == 1) message += "WGS84)";
                if (Val1 == 2) message += "SK42)";
                if (Val1 == 3) message += "Merkator)";

            }
            else
                message += "untiteled type";
            return message;
        }


    }
}
