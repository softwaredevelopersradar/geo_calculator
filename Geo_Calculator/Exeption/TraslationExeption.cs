﻿using System;


namespace Geo_Calculator
{
    class TraslationExeption : Exception
    {
        byte Val1 { get; set; } 
        public string  Message1 {get; set;}
        public TraslationExeption(string message, byte Val1)
        {
            this.Val1 = Val1;
            Message1 =  CreateMessage(Val1) + message + ". ";
        }
        public static string CreateMessage(byte Val1)
        {
            string message = "";
            if (Val1 == 1) message += "WGS84";
            if (Val1 == 2) message += "SK42";
            if (Val1 == 3) message += "Merkator";
            return message;
        }

    }
}
