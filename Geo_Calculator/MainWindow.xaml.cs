﻿using System.Windows;
using System.Collections;
using System.Collections.ObjectModel;
using System.Windows.Input;
using System.Globalization;

namespace Geo_Calculator
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        int Counter;
        ArrayList list;
        ObservableCollection<StructerOfData> list2;
        
       public static double dy, dz, dx;
        double Lat, Long;
        double MinLong = 0;
        double MinLat = 0;
        double SekLong = 0;
        double SekLat = 0;
        int IntLong2 = 0, IntLat2 = 0;
        int IntMinLong2 = 0, IntMinLat2 = 0;
        char[] charsToTrim = { ' ', '\n', ',' };

        public static byte SystemOfCoordinate1 { get; set; } = 1;
        public static byte SystemOfCoordinate2 { get; set; } = 2;

        public static byte TypeOfEnter { get; set; } = 2;
        public static byte TypeOfDisplay { get; set; } = 2;

        public MainWindow()
        {
            dx = 25; dy = -141; dz = -80;
            InitializeComponent();
            list = new ArrayList();
            list2 = new ObservableCollection<StructerOfData>();
            Counter = 0;
            TextBoxLeftUp1.Focus();
            CultureInfo.CurrentCulture = new CultureInfo("en-EN", false);
        }

        private void RadioButton_Checked_23(object sender, RoutedEventArgs e) => TypeOfDisplay = 2;
        private void RadioButton_Checked_33(object sender, RoutedEventArgs e) => TypeOfDisplay = 3;
        private void RadioButton_Checked_34(object sender, RoutedEventArgs e) => TypeOfDisplay = 4;

        private void Button_Click_1(object sender, RoutedEventArgs e)
        {
            TextBoxLeftUp1.Clear();
            TextBoxLeftUp2.Clear();
            TextBoxLeftUp3.Clear();
            TextBoxLeftDown1.Clear();
            TextBoxLeftDown3.Clear();
            TextBoxLeftDown2.Clear();
            ClearTextBox();
            MinLong = 0;
            MinLat = 0;
            Long = 0;
            Lat = 0;
            SekLat = 0;
            SekLong = 0;
        }

        private void Expander_Expanded(object sender, RoutedEventArgs e) => Win.Height = 590;

        private void Expander_Collapsed(object sender, RoutedEventArgs e) => Win.Height = Win.MinHeight;
    }

}
