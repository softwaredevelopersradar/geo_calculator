﻿using System;
using System.Windows;

namespace Geo_Calculator
{
    public partial class MainWindow : Window
    {
        private void Button_Click_Translate(object sender, RoutedEventArgs e)
        {           
            ErrorLabel.Content = "";
           
            double NewLat=0, NewLong=0;

            try {Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
            catch (Exception) { ErrorLabel.Content = "Некорректный ввод"; return; }
            try { Lat = Convert.ToDouble(TextBoxLeftDown1.Text); }
            catch (Exception) { ErrorLabel.Content = "Некорректный ввод"; return; }

            try { MinLong = Convert.ToDouble(TextBoxLeftUp2.Text); }
            catch (Exception) { MinLong = 0; }
            try { MinLat = Convert.ToDouble(TextBoxLeftDown2.Text); }
            catch (Exception) { MinLat = 0; }
            try { SekLong = Convert.ToDouble(TextBoxLeftUp3.Text); }
            catch (Exception) { SekLong = 0; }
            try { SekLat = Convert.ToDouble(TextBoxLeftDown3.Text); }
            catch (Exception) { SekLat = 0; }

            StructerOfData SOD = new StructerOfData();
            Counter++; SOD.Num = "  " + Convert.ToString(Counter);
            SOD.Lat = TextBoxLeftUp1.Text + "°";
            SOD.Long = TextBoxLeftDown1.Text + "°";

            if (SystemOfCoordinate1 != 3)
            {
                switch (TypeOfEnter)
                {
                    case 2: break;
                    case 3:
                        SOD.Lat = TextBoxLeftUp1.Text + "°" + TextBoxLeftUp2.Text + "'";
                        SOD.Long = TextBoxLeftDown1.Text + "°" + TextBoxLeftDown2.Text + "'";
                        GeoCalculator.ClassGeoCalculator.f_GM_Grad((int)Long, MinLong, ref Long);
                        GeoCalculator.ClassGeoCalculator.f_GM_Grad((int)Lat, MinLat, ref Lat);
                        break;
                    case 4:
                        SOD.Lat = TextBoxLeftUp1.Text + "°" + TextBoxLeftUp2.Text + "'" + TextBoxLeftUp3.Text + "\"";
                        SOD.Long = TextBoxLeftDown1.Text + "°" + TextBoxLeftDown2.Text + "'" + TextBoxLeftDown3.Text + "\"";
                        GeoCalculator.ClassGeoCalculator.f_GMS_Grad((int)Long, (int)MinLong, SekLong, ref Long);
                        GeoCalculator.ClassGeoCalculator.f_GMS_Grad((int)Lat, (int)MinLat, SekLat, ref Lat);
                        break;
                    default: return;
                }
            }
            else
            {
                switch (TypeOfEnter)
                {
                    case 2:
                        SOD.Lat = TextBoxLeftUp1.Text + "м.";
                        SOD.Long = TextBoxLeftDown1.Text + "м.";
                        break;
                    case 3:
                        SOD.Lat = TextBoxLeftUp1.Text + "км.";
                        SOD.Long = TextBoxLeftDown1.Text + "км.";
                        Lat = Lat * 1000;
                        Long = Long * 1000;
                        break;
                    default: return;
                }
            }

            try
            {
                switch (SystemOfCoordinate1)
                {
                    case 1:
                        InputData1(SOD, Lat, Long);
                                SOD.MGRS = TextBoxRightDown_Copy.Text = GeoCalculator.ClassGeoCalculator.f_WGS84_MGRS(Lat, Long);
                                GeoCalculator.ClassGeoCalculator.f_WGS84_SK42_BL(Lat, Long, dx, dy, dz, ref NewLat, ref NewLong);
                        InputData2(SOD, NewLat, NewLong);
                        NewLat = NewLong = 0;

                                SOD.MGRS = TextBoxRightDown_Copy.Text = GeoCalculator.ClassGeoCalculator.f_WGS84_MGRS(Lat, Long);
                                GeoCalculator.ClassGeoCalculator.f_WGS84_Mercator(Lat, Long, ref NewLat, ref NewLong);
                        InputData3(SOD, NewLat, NewLong);
                        NewLat = NewLong = 0;
                        break;
                    case 2:
                        InputData2(SOD, Lat, Long);
                                GeoCalculator.ClassGeoCalculator.f_SK42_WGS84_BL(Lat, Long, dx, dy, dz, ref NewLat, ref NewLong);
                                SOD.MGRS = TextBoxRightDown_Copy.Text = GeoCalculator.ClassGeoCalculator.f_WGS84_MGRS(NewLat, NewLong);
                        InputData1(SOD, NewLat, NewLong);
                        NewLat = NewLong = 0;
                                GeoCalculator.ClassGeoCalculator.f_SK42_WGS84_BL(Lat, Long, dx, dy, dz, ref NewLat, ref NewLong);
                                SOD.MGRS = TextBoxRightDown_Copy.Text = GeoCalculator.ClassGeoCalculator.f_WGS84_MGRS(NewLat, NewLong);
                                GeoCalculator.ClassGeoCalculator.f_WGS84_Mercator(NewLat, NewLong, ref NewLat, ref NewLong);
                        InputData3(SOD, NewLat, NewLong);
                        NewLat = NewLong = 0;

                        break;
                    case 3:
                        InputData3(SOD, Lat, Long);
                                GeoCalculator.ClassGeoCalculator.f_Mercator_WGS84(Lat, Long, ref NewLat, ref NewLong);
                                SOD.MGRS = TextBoxRightDown_Copy.Text = GeoCalculator.ClassGeoCalculator.f_WGS84_MGRS(NewLat, NewLong);
                        InputData1(SOD, NewLat, NewLong);
                        NewLat = NewLong = 0;
                                GeoCalculator.ClassGeoCalculator.f_Mercator_WGS84(Lat, Long, ref NewLat, ref NewLong);
                                SOD.MGRS = TextBoxRightDown_Copy.Text = GeoCalculator.ClassGeoCalculator.f_WGS84_MGRS(NewLat, NewLong);
                                GeoCalculator.ClassGeoCalculator.f_WGS84_SK42_BL(NewLat, NewLong, dx, dy, dz, ref NewLat, ref NewLong);
                        InputData2(SOD, NewLat, NewLong);
                        NewLat = NewLong = 0;
                        break;
                    default: throw new TraslationExeption("Такой перевод невозможен. \n", SystemOfCoordinate1);
                }
            }
            catch (TraslationExeption error)
            {
                ErrorLabel.Content = error.Message1;
                return;
            }
            catch (OneTypeExeption error)
            {
                ErrorLabel.Content = error.Message1;
                return;
            }

            SOD.Info = "Из " + TraslationExeption.CreateMessage(SystemOfCoordinate1);
           list2.Add(SOD);
            dataGrid.ItemsSource = list2;

   
                string copyText = string.Empty;
                copyText =  "WGS84" + Environment.NewLine + " ( Д: " + (TextBoxRightUp.Text).Trim(charsToTrim) +  ", Ш: " + (TextBoxRightDown.Text).Trim(charsToTrim) + "; " + Environment.NewLine + "Д: " + (TextBoxRightUp1.Text).Trim(charsToTrim) + ", Ш: " + (TextBoxRightDown1.Text).Trim(charsToTrim) + "; " + Environment.NewLine  + "Д: " + (TextBoxRightUp2.Text).Trim(charsToTrim) + ", Ш: " + (TextBoxRightDown2.Text).Trim(charsToTrim) + " ), " + Environment.NewLine
                    + "СК42" + Environment.NewLine + " ( Д: " + (TextBoxRightUp02.Text).Trim(charsToTrim) + ", Ш: " + (TextBoxRightDown02.Text).Trim(charsToTrim) + "; " + Environment.NewLine + "Д: " + (TextBoxRightUp12.Text).Trim(charsToTrim) + ", Ш: " + (TextBoxRightDown12.Text).Trim(charsToTrim) + "; " + Environment.NewLine + "Д: " + (TextBoxRightUp22.Text).Trim(charsToTrim) + ", Ш: " + (TextBoxRightDown22.Text).Trim(charsToTrim) + " ), " + Environment.NewLine
                    + "Меркатор" + Environment.NewLine + " ( Д: " + (TextBoxRightUp03.Text).Trim(charsToTrim) + ", Ш: " + (TextBoxRightDown03.Text).Trim(charsToTrim) + Environment.NewLine + "; " + "Д: " + (TextBoxRightUp13.Text).Trim(charsToTrim) + ", Ш: " + (TextBoxRightDown13.Text).Trim(charsToTrim) + " ), " + Environment.NewLine 
                    + "MGRS: " + (TextBoxRightDown_Copy.Text).Trim();
                Clipboard.SetDataObject(copyText);
        }
        private void ClearTextBox()
        {
            TextBoxRightDown.Clear();
            TextBoxRightDown1.Clear();
            TextBoxRightDown2.Clear();
            TextBoxRightDown02.Clear();
            TextBoxRightDown12.Clear();
            TextBoxRightDown03.Clear();
            TextBoxRightDown13.Clear();
            TextBoxRightDown22.Clear();
            TextBoxRightUp.Clear();
            TextBoxRightUp1.Clear();
            TextBoxRightUp2.Clear();
            TextBoxRightUp02.Clear();
            TextBoxRightUp12.Clear();
            TextBoxRightUp03.Clear();
            TextBoxRightUp13.Clear();
            TextBoxRightUp22.Clear();
            TextBoxRightDown_Copy.Clear();
        }

        private void InputData1(StructerOfData SOD, double Lat, double Long)
        {
         Lat = Math.Round(Lat, 6, MidpointRounding.AwayFromZero);
         Long = Math.Round(Long, 6, MidpointRounding.AwayFromZero);
                SOD.WGS84 += "Д: ";
                SOD.WGS84 += TextBoxRightUp.Text = Convert.ToString(Long) + "°   ";
                SOD.WGS84 += "Ш: ";
                SOD.WGS84 += TextBoxRightDown.Text = Convert.ToString(Lat) + "°  \n";

                GeoCalculator.ClassGeoCalculator.f_Grad_GM(Long, ref IntLong2, ref MinLong);
                GeoCalculator.ClassGeoCalculator.f_Grad_GM(Lat, ref IntLat2, ref MinLat);
            MinLat = Math.Round(MinLat, 4, MidpointRounding.AwayFromZero);
            MinLong = Math.Round(MinLong, 4, MidpointRounding.AwayFromZero);
                SOD.WGS84 += "Д: ";
                SOD.WGS84 += TextBoxRightUp1.Text = Convert.ToString(IntLong2) + "°" + Convert.ToString(MinLong) + "'" + "   ";
                SOD.WGS84 += "Ш: "; 
                SOD.WGS84 += TextBoxRightDown1.Text = Convert.ToString(IntLat2) + "°" + Convert.ToString(MinLat) + "'" + "  \n";

                GeoCalculator.ClassGeoCalculator.f_Grad_GMS(Long, ref IntLong2, ref IntMinLong2, ref SekLong);
                GeoCalculator.ClassGeoCalculator.f_Grad_GMS(Lat, ref IntLat2, ref IntMinLat2, ref SekLat);
            SekLat = Math.Round(SekLat, 2, MidpointRounding.AwayFromZero);
            SekLong = Math.Round(SekLong, 2, MidpointRounding.AwayFromZero);
                SOD.WGS84 += "Д: ";
                SOD.WGS84 += TextBoxRightUp2.Text = Convert.ToString(IntLong2) + "°" + Convert.ToString(IntMinLong2) + "'" + Convert.ToString(SekLong) + "\"" + "   ";
                SOD.WGS84 += "Ш: ";
                SOD.WGS84 += TextBoxRightDown2.Text = Convert.ToString(IntLat2) + "°" + Convert.ToString(IntMinLat2) + "'" + Convert.ToString(SekLong) + "\"" + "  \n";
                
        }
        private void InputData2(StructerOfData SOD, double Lat, double Long)
        {
            Lat = Math.Round(Lat, 6, MidpointRounding.AwayFromZero);
            Long = Math.Round(Long, 6, MidpointRounding.AwayFromZero);
            SOD.CK42 += "Д: ";
            SOD.CK42 += TextBoxRightUp02.Text = Convert.ToString(Long) + "°   ";
            SOD.CK42 += "Ш: ";
            SOD.CK42 += TextBoxRightDown02.Text = Convert.ToString(Lat) + "°  \n";

            GeoCalculator.ClassGeoCalculator.f_Grad_GM(Long, ref IntLong2, ref MinLong);
            GeoCalculator.ClassGeoCalculator.f_Grad_GM(Lat, ref IntLat2, ref MinLat);
            MinLat = Math.Round(MinLat, 4, MidpointRounding.AwayFromZero);
            MinLong = Math.Round(MinLong, 4, MidpointRounding.AwayFromZero);
            SOD.CK42 += "Д: ";
            SOD.CK42 += TextBoxRightUp12.Text = Convert.ToString(IntLong2) + "°" + Convert.ToString(MinLong) + "'" + "   ";
            SOD.CK42 += "Ш: ";
            SOD.CK42 += TextBoxRightDown12.Text = Convert.ToString(IntLat2) + "°" + Convert.ToString(MinLat) + "'" + "  \n";

            GeoCalculator.ClassGeoCalculator.f_Grad_GMS(Long, ref IntLong2, ref IntMinLong2, ref SekLong);
            GeoCalculator.ClassGeoCalculator.f_Grad_GMS(Lat, ref IntLat2, ref IntMinLat2, ref SekLat);
            SekLat = Math.Round(SekLat, 2, MidpointRounding.AwayFromZero);
            SekLong = Math.Round(SekLong, 2, MidpointRounding.AwayFromZero);
            SOD.CK42 += "Д: ";
            SOD.CK42 += TextBoxRightUp22.Text = Convert.ToString(IntLong2) + "°" + Convert.ToString(IntMinLong2) + "'" + Convert.ToString(SekLong) + "\"" + "   ";
            SOD.CK42 += "Ш: ";
            SOD.CK42 += TextBoxRightDown22.Text = Convert.ToString(IntLat2) + "°" + Convert.ToString(IntMinLat2) + "'" + Convert.ToString(SekLong) + "\"" + "  \n";

        }
        private void InputData3(StructerOfData SOD, double Lat, double Long)
        {
            Lat = Math.Round(Lat, 0, MidpointRounding.AwayFromZero);
            Long = Math.Round(Long, 0, MidpointRounding.AwayFromZero);
            SOD.Merkator += "Д: ";
            SOD.Merkator += TextBoxRightUp03.Text = Convert.ToString(Long) + " м." + "   ";
            SOD.Merkator += "Ш: ";
            SOD.Merkator += TextBoxRightDown03.Text = Convert.ToString(Lat) + " м." + "  \n";

            Long = Long * 0.001;
            Lat = Lat * 0.001;
            SOD.Merkator += "Д: ";
            SOD.Merkator += TextBoxRightUp13.Text = Convert.ToString(Long) + " км." + "   ";
            SOD.Merkator += "Ш: ";
            SOD.Merkator += TextBoxRightDown13.Text = Convert.ToString(Lat) + " км." + "  \n";

        }
    }
}
