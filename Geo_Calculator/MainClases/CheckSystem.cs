﻿using System.Windows;



namespace Geo_Calculator
{
    public partial class MainWindow : Window
    {
        private void Button_Click(object sender, RoutedEventArgs e)
        {
            byte Temp1 = SystemOfCoordinate1;
            SystemOfCoordinate1 = SystemOfCoordinate2;
            SystemOfCoordinate2 = Temp1;
            CheckSystemOfCoordinate();

            byte Temp2 = TypeOfEnter;
            TypeOfEnter = TypeOfDisplay;
            TypeOfDisplay = Temp2;
            CheckTypeOfEnterAndDisplay();
        }

        private void CheckTypeOfEnterAndDisplay()
        {
            switch (TypeOfEnter)
            {
                case 2:
                    R5C.IsChecked = true;
                    break;
                case 3:
                    R6C.IsChecked = true;
                    break;
                case 4:
                    R7C.IsChecked = true;
                    break;
            }
        }

        private void CheckSystemOfCoordinate()
        {
            switch (SystemOfCoordinate1)
            {
                case 1:
                    R1.IsChecked = true;
                    break;
                case 2:
                    R2.IsChecked = true;
                    break;
                case 3:
                    R3.IsChecked = true;
                    break;
            }
        }
    }
}
