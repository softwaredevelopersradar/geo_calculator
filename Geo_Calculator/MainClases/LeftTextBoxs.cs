﻿using System.Windows;
using System.Windows.Input;


namespace Geo_Calculator
{
    public partial class MainWindow : Window
    {
        private void TextBoxLeftUp1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            if (R5C.IsChecked == true)
            {
                if (e.Key == Key.Space)
                {
                    TextBoxLeftDown1.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftDown1.Focus();
                }
            }
            else
            {
                if (e.Key == Key.Space)
                {
                    TextBoxLeftUp2.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftDown1.Focus();
                }
            }
        }
        private void TextBoxLeftUp2_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            if (R6C.IsChecked == true)
            {
                if (e.Key == Key.Space)
                {
                    TextBoxLeftDown1.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftDown2.Focus();
                }
            }
            else
            {   if (e.Key == Key.Space)
                {
                    TextBoxLeftUp3.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftDown2.Focus();
                }
            }
        }
        private void TextBoxLeftUp3_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            if (e.Key == Key.Space)
            {
                TextBoxLeftDown1.Focus();
            }
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                TextBoxLeftDown3.Focus();
            }
        }
        private void TextBoxLeftDown1_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            if (R5C.IsChecked == true)
            {
                if (e.Key == Key.Space)
                {
                    TextBoxLeftUp1.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftUp1.Focus();
                }
            }
            else
            {
                if (e.Key == Key.Space)
                {
                    TextBoxLeftDown2.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftUp1.Focus();
                }
            }
        }
        private void TextBoxLeftDown2_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            if (R6C.IsChecked == true)
            {
                if (e.Key == Key.Space)
                {
                    TextBoxLeftUp1.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftUp2.Focus();
                }
            }
            else
            {
                if (e.Key == Key.Space)
                {
                    TextBoxLeftDown3.Focus();
                }
                if (e.Key == Key.Up || e.Key == Key.Down)
                {
                    TextBoxLeftUp2.Focus();
                }
            }
        }
        private void TextBoxLeftDown3_PreviewKeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.Space)
                e.Handled = true;
            if (e.Key == Key.Space)
            {
                TextBoxLeftUp1.Focus();
            }
            if (e.Key == Key.Up || e.Key == Key.Down)
            {
                TextBoxLeftUp3.Focus();
            }
        }
    }
}
