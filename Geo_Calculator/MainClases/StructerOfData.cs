﻿using System;

namespace Geo_Calculator
{
    public class StructerOfData
    {
        public string Num { get; set; }
        public string Info { get; set; }

        public string Long { get; set; }
        public string Lat { get; set; }

        public string WGS84 { get; set; }
        public string CK42 { get; set; }
        public string Merkator {get; set; }

        public string MGRS { get; set; }
         
        public override string ToString()
        { 

            return "\n" + Convert.ToString(Num) + "  " + Long + "  " + Lat  + "    "+ WGS84 + "    " + CK42 + "   " + Merkator + "   " + MGRS;
        }
    }
}
