﻿using System.Windows;
using System.Windows.Controls;
using System.Windows.Input;
using System.Text.RegularExpressions;


namespace Geo_Calculator
{
    public partial class MainWindow : Window
    {
        private Regex regex1 = new Regex(@"^[0-9]{0,3}($|([.]{1}))[0-9]{0,6}$");
        private Regex regex2 = new Regex(@"^[0-9]{0,2}($|([.]{1}))[0-9]{0,4}$");
        private Regex regex2_1 = new Regex(@"^[0-9]{0,2}($|([.]{1}))[0-9]{0,2}$");
        private Regex regex1M = new Regex(@"^[0-9]{0,4}($|([.]{1}))[0-9]{0,3}$");
        private Regex regex2M = new Regex(@"^[0-9]{0,7}$");
        private Regex regex3 = new Regex(@"^[0-9]{0,3}$");
        private Regex regex4 = new Regex(@"^[0-9]{0,2}$");
        //private Regex regex5 = new Regex(@"^[-]{0,1}[0-9]{0,3}($|([.]{1}))[0-9]*$");
        private void TextBox_PreviewTextInput3(object sender, TextCompositionEventArgs e)
        {
            if (TypeOfEnter == 4)
                e.Handled = !regex2_1.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
            else e.Handled = !regex4.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
        }

        private void TextBox_PreviewTextInput2(object sender, TextCompositionEventArgs e)
        {
            if (TypeOfEnter == 3)
                e.Handled = !regex2.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
            else e.Handled = !regex4.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));  
        }

        private void TextBox_PreviewTextInput11(object sender, TextCompositionEventArgs e)
        {
            if (SystemOfCoordinate1 != 3)
            {
                if (TypeOfEnter == 2)
                    e.Handled = !regex1.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
                else e.Handled = !regex3.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
            }
            else
            {
                if (TypeOfEnter == 3)
                    e.Handled = !regex1M.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
                else e.Handled = !regex2M.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
            }
        }

        private void TextBox_PreviewTextInput12(object sender, TextCompositionEventArgs e)
        {
            if (SystemOfCoordinate1 != 3)
            {
                if (TypeOfEnter == 2)
                    e.Handled = !regex2.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
                else e.Handled = !regex4.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
            }
            else
            {
                if (TypeOfEnter == 3)
                    e.Handled = !regex1M.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
                else e.Handled = !regex2M.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
            }
        }

        //private void TextBox_PreviewTextInput(object sender, TextCompositionEventArgs e)
        //{
          //  e.Handled = !regex5.IsMatch((sender as TextBox).Text.Insert((sender as TextBox).SelectionStart, e.Text));
       // }
    }
}
