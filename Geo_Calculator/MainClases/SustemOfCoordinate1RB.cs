﻿using System.Windows;

namespace Geo_Calculator
{
    public partial class MainWindow : Window
    {

        private void RadioButton_Checked_2(object sender, RoutedEventArgs e)
        {
            if(SystemOfCoordinate1 == 3)
            {
                R5C.IsChecked = true;
            }

            SystemOfCoordinate1 = 1;
            R5C.Content = "гр.";
            R6C.Content = "гр. мин.";
            R7C.Content = "гр. мин. сек.";
            R7C.Visibility = Visibility.Visible;
            TextBoxLeftUp2.IsReadOnly = false;
            TextBoxLeftUp3.IsReadOnly = false;
            TextBoxLeftDown2.IsReadOnly = false;
            TextBoxLeftDown3.IsReadOnly = false;

            Label1.Content = "°  ";
            Label2.Content = "°  ";
        }
        private void RadioButton_Checked_3(object sender, RoutedEventArgs e)
        {
            if (SystemOfCoordinate1 == 3)
            {
                R5C.IsChecked = true;
            }
            SystemOfCoordinate1 = 2;
            R5C.Content = "гр.";
            R6C.Content = "гр. мин.";
            R7C.Content = "гр. мин. сек.";
            R7C.Visibility = Visibility.Visible;
            TextBoxLeftUp2.IsReadOnly = false;
            TextBoxLeftUp3.IsReadOnly = false;
            TextBoxLeftDown2.IsReadOnly = false;
            TextBoxLeftDown3.IsReadOnly = false;

            if (TypeOfEnter == 3)
            {
                TextBoxLeftUp2.Visibility = Visibility.Visible;
                TextBoxLeftDown2.Visibility = Visibility.Visible;
                Label11.Visibility = Visibility.Visible;
                Label21.Visibility = Visibility.Visible;
            }

            Label1.Content = "°  ";
            Label2.Content = "°  ";
        }
        private void RadioButton_Checked_4(object sender, RoutedEventArgs e)
        {
            SystemOfCoordinate1 = 3;
            R5C.Content = "Метры";
            R6C.Content = "Километры";
            R7C.Content = "";

            TextBoxLeftUp2.IsReadOnly = true;
            TextBoxLeftUp3.IsReadOnly = true;
            TextBoxLeftDown2.IsReadOnly = true;
            TextBoxLeftDown3.IsReadOnly = true;
            R7C.Visibility = Visibility.Collapsed;

            Label1.Content = "м.";
            Label2.Content = "м.";
            R5C.IsChecked = true;
        }
    }
}
