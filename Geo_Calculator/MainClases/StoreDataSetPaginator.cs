﻿using System;
using System.Collections.Generic;
using System.Collections.ObjectModel;
using System.Drawing;
using System.Globalization;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows;
using System.Windows.Documents;
using System.Windows.Media;


namespace Geo_Calculator.MainClases
{
    class StoreDataSetPaginator : DocumentPaginator

    {
        ObservableCollection<StructerOfData> list2;
        private Typeface typeface;
        private double fontSize;
        private double margin;
        private int Counter;
        char[] charsToTrim = { ' ', '\n', ',' };

        private System.Windows.Size pageSize;
        public override System.Windows.Size PageSize
        {
            get
            {
                return pageSize;
            }
            set
            {
                pageSize = value;
                PaginateData();
            }
        }

        public StoreDataSetPaginator(ObservableCollection<StructerOfData> list2, Typeface typeface, double fontSize, double margin, System.Windows.Size pageSize, int Counter)
        {
            this.list2 = list2;
            this.typeface = typeface;
            this.fontSize = fontSize;
            this.margin = margin;
            this.pageSize = pageSize;
            this.Counter = Counter;
            PaginateData();
        }

        private int pageCount;
        private int rowsPerPage;
        private void PaginateData()
        {
            // Создать тестовую строку для измерения
            FormattedText text = GetFormattedText("A");

            // Подсчитать строки, которые умещаются на странице
            rowsPerPage = (int)((pageSize.Height - margin * 2) / text.Height);

            // Оставить строку для заголовка 
            rowsPerPage -= 1;

            pageCount = Counter / 8;
        }

        private FormattedText GetFormattedText(string text)
        {
            return GetFormattedText(text, typeface);
        }

        private FormattedText GetFormattedText(string text, Typeface typeface)
        {
            return new FormattedText(
                 text, CultureInfo.CurrentCulture, FlowDirection.LeftToRight,
                      typeface, fontSize, System.Windows.Media.Brushes.Black);
        }
        public override bool IsPageCountValid
        {
            get { return true; }
        }

        public override int PageCount
        {
            get { return pageCount; }
        }

        public override IDocumentPaginatorSource Source
        {
            get { return null; }
        }

        public override DocumentPage GetPage(int pageNumber)
        {
            // Создать тестовую строку для измерения


            // Размеры столбцов относительно ширины символа "A"
       
            DrawingVisual visual = new DrawingVisual();

            // Установить позицию в верхний левый угол печатаемой области
            
            using (DrawingContext dc = visual.RenderOpen())
            {
                // Нарисовать заголовки столбцов
                Typeface columnHeaderTypeface = new Typeface(typeface.FontFamily, FontStyles.Normal, FontWeights.Bold, FontStretches.Normal);

                string txtContent = " ";

                /*string text1 = "";
                text1 += "    " + "N°" + "   " + list2[0].Num + "\n" + "Описание   " + list2[0].Info.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "Широта   " + list2[0].Lat.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "Долгота   " + list2[0].Long.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "WGS84   " + list2[0].WGS84.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "CK42   " + list2[0].CK42.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "Меркатор   " + list2[0].Merkator.Trim(charsToTrim).Replace("\n", " /  ") + "\n" +  "MGRS   " + list2[0].MGRS.Trim(charsToTrim).Replace("\n", " /  ") + "\n";
                FormattedText text12 = new FormattedText(text1,
                     System.Globalization.CultureInfo.CurrentCulture,
                     FlowDirection.LeftToRight,
                     new Typeface("Calibri"), 11, System.Windows.Media.Brushes.Black);*/

                FormattedText text12 = GetFormattedText("A");

                // Указать максимальную ширину, в пределах которой выполнять перенос текста, 
                text12.MaxTextWidth = pageSize.Width / 1.1;

                // Получить размер выводимого текста. 
                System.Windows.Size textSize = new System.Windows.Size(text12.Width, text12.Height);

                // Найти верхний левый угол, куда должен быть помещен текст. 

                System.Windows.Point point1 = new System.Windows.Point(
                    (pageSize.Width - textSize.Width) / 2 - margin,
                    (pageSize.Height - textSize.Height) / 2 - margin);

                // Нарисовать содержимое,

                // Добавить рамку (прямоугольник без фона). 
               
                for (int i = 0; i < Counter; i++)
                {
                    txtContent += "    " + "N°" + "   " + list2[i].Num + "\n" + "Описание   " + list2[i].Info.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "Широта   " + list2[i].Lat.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "Долгота   " + list2[i].Long.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "WGS84   " + list2[i].WGS84.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "CK42   " + list2[i].CK42.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "Меркатор   " + list2[i].Merkator.Trim(charsToTrim).Replace("\n", " /  ") + "\n" + "MGRS   " + list2[i].MGRS.Trim(charsToTrim).Replace("\n", " /  ") + "\n";
                    if (i % 8 == 0)
                    {
                        dc.DrawRectangle(null, new System.Windows.Media.Pen(System.Windows.Media.Brushes.Black, 1),
                   new Rect(margin, margin, pageSize.Width - margin * 2, pageSize.Height - margin * 2));
                        FormattedText text = new FormattedText(txtContent,
                      System.Globalization.CultureInfo.CurrentCulture,
                      FlowDirection.LeftToRight,
                      new Typeface("Calibri"), 11, System.Windows.Media.Brushes.Black);
                        dc.DrawText(text, point1);
                        txtContent = " ";
                    }
                  
                }

            }
            return new DocumentPage(visual, pageSize, new Rect(pageSize), new Rect(pageSize));
        }
    }
    }
