﻿using System;
using System.Windows;
using System.Windows.Input;
using System.Text.RegularExpressions;


namespace Geo_Calculator
{
    public partial class MainWindow : Window
    {

        private void Gri_KeyUp(object sender, System.Windows.Input.KeyEventArgs e)
        {
            if (e.Key == Key.Enter)
            {
                Button_Click_Translate(new object(), new RoutedEventArgs());
                TextBoxLeftUp1.Focus();
            }
        }
        private void Gri_KeyDown(object sender, KeyEventArgs e)
        {
            if (e.Key == Key.V)
            {
                    string Buffer = Clipboard.GetText();
                    string pattern1 = @"\d{1,3}°\d{1,2}'\d{1,2}.\d.*";
                    string pattern2 = @"\d{1,3}°\d{1,2}.\d.*";
                    string pattern3 = @"\d{1,3}.\d.*";

                    if ((TextBoxLeftUp1.IsKeyboardFocusWithin == true || TextBoxLeftUp2.IsKeyboardFocusWithin == true || TextBoxLeftUp3.IsKeyboardFocusWithin == true) && (SystemOfCoordinate1 == 3))
                    {
                        if (Regex.IsMatch(Buffer, @"\d{1,3}.\d."))
                        {
                            string c1 = Buffer.Substring(Buffer.Length - 3);
                          
                            try
                            {
                                if (Buffer.IndexOf(".") >= 4)
                                    R5C.IsChecked = true;
                            }
                            catch (Exception)
                            {
                                if (Buffer.Length >= 4)
                                    R5C.IsChecked = true;
                            }

                            if (c1[0] < '0' || c1[0] > '9')
                                TextBoxLeftUp1.Text = (Buffer.Substring(0, Buffer.Length - 3)).Trim();
                            else if (c1[1] < '0' || c1[1] > '9')
                                TextBoxLeftUp1.Text = (Buffer.Substring(0, Buffer.Length - 2)).Trim();
                            else if (c1[0] < '0' || c1[0] > '9')
                                TextBoxLeftUp1.Text = (Buffer.Substring(0, Buffer.Length - 1)).Trim();
                            else TextBoxLeftUp1.Text = Buffer;
                        }
                        else return;
                    }
                    else if ((TextBoxLeftDown1.IsKeyboardFocusWithin == true || TextBoxLeftDown2.IsKeyboardFocusWithin == true || TextBoxLeftDown3.IsKeyboardFocusWithin == true) && (SystemOfCoordinate1 == 3))
                    {
                        if (Regex.IsMatch(Buffer, @"\d{1,3}.\d."))
                        {
                            string c1 = Buffer.Substring(Buffer.Length - 3);
                            try
                            {
                                if (Buffer.IndexOf(".") >= 4)
                                    R5C.IsChecked = true;
                            }
                            catch (Exception)
                            {
                                if (Buffer.Length >= 4)
                                    R5C.IsChecked = true;
                            }

                            if (c1[0] < '0' || c1[0] > '9')
                                TextBoxLeftDown1.Text = (Buffer.Substring(0, Buffer.Length - 3)).Trim();
                            else if (c1[1] < '0' || c1[1] > '9')
                                TextBoxLeftDown1.Text = (Buffer.Substring(0, Buffer.Length - 2)).Trim();
                            else if (c1[0] < '0' || c1[0] > '9')
                                TextBoxLeftDown1.Text = (Buffer.Substring(0, Buffer.Length - 1)).Trim();
                            else TextBoxLeftDown1.Text = Buffer;
                        }
                        else return;
                    }
                    else if ((TextBoxLeftDown1.IsKeyboardFocusWithin == true || TextBoxLeftDown2.IsKeyboardFocusWithin == true || TextBoxLeftDown3.IsKeyboardFocusWithin == true || TextBoxLeftUp1.IsKeyboardFocusWithin == true || TextBoxLeftUp2.IsKeyboardFocusWithin == true || TextBoxLeftUp3.IsKeyboardFocusWithin == true) && (Regex.IsMatch(Buffer, pattern1 + @"\S{0,1}\s{1,}\S*\s{1,}" + pattern1)) || (Regex.IsMatch(Buffer, pattern2 + @"\S{0,1}\s{1,}\S*\s{1,}" + pattern2) || (Regex.IsMatch(Buffer, pattern3 + @"\S{0,1}\s{1,}\S*\s{1,}" + pattern3))))
                    {
                        Regex reg = new Regex(@"\s{1,}\S*\s{1,}");
                        string[] substrings = reg.Split(Buffer);
                        bufferDisp1(substrings[0]);
                        bufferDisp2(substrings[1].Substring(0, substrings[1].Length));
                    }
                    else if ((TextBoxLeftDown1.IsKeyboardFocusWithin == true || TextBoxLeftDown2.IsKeyboardFocusWithin == true || TextBoxLeftDown3.IsKeyboardFocusWithin == true || TextBoxLeftUp1.IsKeyboardFocusWithin == true || TextBoxLeftUp2.IsKeyboardFocusWithin == true || TextBoxLeftUp3.IsKeyboardFocusWithin == true) && (Regex.IsMatch(Buffer, pattern1 + @"\S{1,2}\s{1,}" + pattern1)) || (Regex.IsMatch(Buffer, pattern2 + @"\S{1,2}\s{1,}" + pattern2) || (Regex.IsMatch(Buffer, pattern3 + @"\S{1,2}\s{1,}" + pattern3))))
                    {
                        Regex reg = new Regex(@"\S{1}\s{1,}");
                        string[] substrings = reg.Split(Buffer);
                        bufferDisp1(substrings[0]);
                        bufferDisp2(substrings[1].Substring(0, substrings[1].Length));
                    }
                    else if (TextBoxLeftUp1.IsKeyboardFocusWithin == true || TextBoxLeftUp2.IsKeyboardFocusWithin == true || TextBoxLeftUp3.IsKeyboardFocusWithin == true)
                    {
                        bufferDisp1(Buffer);
                    }
                    else if (TextBoxLeftDown1.IsKeyboardFocusWithin == true || TextBoxLeftDown2.IsKeyboardFocusWithin == true || TextBoxLeftDown3.IsKeyboardFocusWithin == true)
                    {
                        bufferDisp2(Buffer);
                    }
                    else return;
            }
        }
        private void bufferDisp1(string Buffer)
        {
            if (Regex.IsMatch(Buffer, @"\d{1,3}°\d{1,2}'\d{1,2}.\d*"))
            {
                char c = Convert.ToChar(Buffer.Substring(Buffer.Length - 1));
                R7C.IsChecked = true;
                TextBoxLeftUp1.Text = Buffer.Substring(0, Buffer.IndexOf('°'));
                TextBoxLeftUp2.Text = Buffer.Substring(Buffer.IndexOf('°') + 1, Buffer.IndexOf('\'') - Buffer.IndexOf('°') - 1);
                if (c < '0' || c > '9')
                    TextBoxLeftUp3.Text = Buffer.Substring(Buffer.IndexOf('\'') + 1, Buffer.Length - Buffer.IndexOf('\'') - 2);
                else TextBoxLeftUp3.Text = Buffer.Substring(Buffer.IndexOf('\'') + 1);
            }
            else if (Regex.IsMatch(Buffer, @"\d{1,3}°\d{1,2}.\d*"))
            {
                char c = Convert.ToChar(Buffer.Substring(Buffer.Length - 1));
                R6C.IsChecked = true;
                TextBoxLeftUp1.Text = Buffer.Substring(0, Buffer.IndexOf('°'));
                if (c < '0' || c > '9')
                    TextBoxLeftUp2.Text = Buffer.Substring(Buffer.IndexOf('°') + 1, Buffer.Length - Buffer.IndexOf('°') - 2);
                else TextBoxLeftUp2.Text = Buffer.Substring(Buffer.IndexOf('°') + 1);
            }
            else if (Regex.IsMatch(Buffer, @"\d{1,3}.\d*"))
            {
                char c = Convert.ToChar(Buffer.Substring(Buffer.Length - 1));
                R5C.IsChecked = true;

                if (c < '0' || c > '9')
                    TextBoxLeftUp1.Text = Buffer.Substring(0, Buffer.Length - 1);
                else TextBoxLeftUp1.Text = Buffer;
            }
            else return;
        }
        private void bufferDisp2(string Buffer)
        {
            if (Regex.IsMatch(Buffer, @"\d{1,3}°\d{1,2}'\d{1,2}.\d*"))
            {
                char c = Convert.ToChar(Buffer.Substring(Buffer.Length - 1));
                R7C.IsChecked = true;
                TextBoxLeftDown1.Text = Buffer.Substring(0, Buffer.IndexOf('°'));
                TextBoxLeftDown2.Text = Buffer.Substring(Buffer.IndexOf('°') + 1, Buffer.IndexOf('\'') - Buffer.IndexOf('°') - 1);
                if (c < '0' || c > '9')
                    TextBoxLeftDown3.Text = Buffer.Substring(Buffer.IndexOf('\'') + 1, Buffer.Length - Buffer.IndexOf('\'') - 2);
                else TextBoxLeftDown3.Text = Buffer.Substring(Buffer.IndexOf('\'') + 1);
            }
            else if (Regex.IsMatch(Buffer, @"\d{1,3}°\d{1,2}.\d*"))
            {
                char c = Convert.ToChar(Buffer.Substring(Buffer.Length - 1));
                R6C.IsChecked = true;
                TextBoxLeftDown1.Text = Buffer.Substring(0, Buffer.IndexOf('°'));
                if (c < '0' || c > '9')
                    TextBoxLeftDown2.Text = Buffer.Substring(Buffer.IndexOf('°') + 1, Buffer.Length - Buffer.IndexOf('°') - 2);
                else TextBoxLeftDown2.Text = Buffer.Substring(Buffer.IndexOf('°') + 1);
            }
            else if (Regex.IsMatch(Buffer, @"\d{1,3}.\d*"))
            {
                char c = Convert.ToChar(Buffer.Substring(Buffer.Length - 1));
                R5C.IsChecked = true;
                TextBoxLeftDown1.Text = Buffer;
                if (c < '0' || c > '9')
                    TextBoxLeftDown1.Text = Buffer.Substring(0, Buffer.Length - 1);
                else TextBoxLeftDown1.Text = Buffer;
            }
            else return;
        }
    }
}
