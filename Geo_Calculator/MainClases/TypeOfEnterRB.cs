﻿using System;
using System.Windows;

namespace Geo_Calculator
{
    public partial class MainWindow : Window
    {
        private void RadioButton_Checked_12(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Content = "";
            while (true)
            {
                if (TypeOfEnter == 3)
                {
                    if (SystemOfCoordinate1 == 3)
                    {
                        try {Lat = Convert.ToDouble(TextBoxLeftDown1.Text);}
                        catch (FormatException)
                        { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1; }

                        try { Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
                        catch (FormatException)
                        { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1;}

                        Long = Long * 1000;
                        Lat = Lat * 1000;
                        TextBoxLeftUp1.Text = Convert.ToString(Long);
                        TextBoxLeftDown1.Text = Convert.ToString(Lat);
                    }
                    else
                    {
                        try{ Long = Convert.ToDouble(TextBoxLeftUp1.Text); }
                        catch (FormatException)
                        { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1; }
                        try { Lat = Convert.ToDouble(TextBoxLeftDown1.Text);}
                        catch (FormatException)
                        {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1;}
                        try { MinLong = Convert.ToDouble(TextBoxLeftUp2.Text);}
                        catch (Exception)
                        { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1;}
                        try { MinLat = Convert.ToDouble(TextBoxLeftDown2.Text); }
                        catch (Exception)
                        { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1; }                    

                        GeoCalculator.ClassGeoCalculator.f_GM_Grad((int)Long, MinLong, ref Long);
                        GeoCalculator.ClassGeoCalculator.f_GM_Grad((int)Lat, MinLat, ref Lat);

                        Lat = Math.Round(Lat, 6, MidpointRounding.AwayFromZero);
                        Long = Math.Round(Long, 6, MidpointRounding.AwayFromZero);

                        TextBoxLeftUp1.Text = Convert.ToString(Long);
                        TextBoxLeftDown1.Text = Convert.ToString(Lat);
                    }
                }
                else if (TypeOfEnter == 4)
                {
                    try { Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
                    catch (FormatException)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1; }
                    try {Lat = Convert.ToDouble(TextBoxLeftDown1.Text); }
                    catch (FormatException)
                    {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}

                    try { MinLong = Convert.ToDouble(TextBoxLeftUp2.Text);}
                    catch (Exception)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp2.Text, TextBoxLeftDown2.Text);
                        ErrorLabel.Content = rb.Message1; }
                    try {MinLat = Convert.ToDouble(TextBoxLeftDown2.Text);}
                    catch (Exception){
                        ExeptionRB rb = new ExeptionRB(TextBoxLeftUp2.Text, TextBoxLeftDown2.Text);
                        ErrorLabel.Content = rb.Message1; }

                    try {SekLong = Convert.ToDouble(TextBoxLeftUp3.Text);}
                    catch (Exception)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp3.Text, TextBoxLeftDown3.Text);
                        ErrorLabel.Content = rb.Message1;}
                    try{ SekLat = Convert.ToDouble(TextBoxLeftDown3.Text);}
                    catch (Exception)
                    {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp3.Text, TextBoxLeftDown3.Text);
                        ErrorLabel.Content = rb.Message1;}

                    GeoCalculator.ClassGeoCalculator.f_GMS_Grad((int)Long, (int)MinLong, SekLong, ref Long);
                    GeoCalculator.ClassGeoCalculator.f_GMS_Grad((int)Lat, (int)MinLat, SekLat, ref Lat);

                    Lat = Math.Round(Lat, 6, MidpointRounding.AwayFromZero);
                    Long = Math.Round(Long, 6, MidpointRounding.AwayFromZero);

                    TextBoxLeftUp1.Text = Convert.ToString(Long);
                    TextBoxLeftDown1.Text = Convert.ToString(Lat);
                }
                break;
            }

            TypeOfEnter = 2;
            TextBoxLeftUp2.Text = "";
            TextBoxLeftUp3.Text = "";
            TextBoxLeftDown2.Text = "";
            TextBoxLeftDown3.Text = "";

            TextBoxLeftUp2.IsReadOnly = true;
            TextBoxLeftUp3.IsReadOnly = true;
            TextBoxLeftDown2.IsReadOnly = true;
            TextBoxLeftDown3.IsReadOnly = true;

            TextBoxLeftUp2.Visibility = Visibility.Collapsed;
            TextBoxLeftUp3.Visibility = Visibility.Collapsed;
            TextBoxLeftDown2.Visibility = Visibility.Collapsed;
            TextBoxLeftDown3.Visibility = Visibility.Collapsed;

            Label11.Visibility = Visibility.Collapsed;
            Label12.Visibility = Visibility.Collapsed;
            Label21.Visibility = Visibility.Collapsed;
            Label22.Visibility = Visibility.Collapsed;

            if (SystemOfCoordinate1 == 3)
            {
                Label1.Content = "м.";
                Label2.Content = "м.";
            }
        }
        private void RadioButton_Checked_13(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Content = "";
            while (true)
            {
                if (TypeOfEnter == 2)
                {
                    if (SystemOfCoordinate1 == 3)
                    {
                        try { Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
                        catch (FormatException)
                        { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1; }
                        try { Lat = Convert.ToDouble(TextBoxLeftDown1.Text);}
                        catch (FormatException)
                        {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1; }
                        TextBoxLeftUp1.Text = Convert.ToString(Long / 1000);
                        TextBoxLeftDown1.Text = Convert.ToString(Lat / 1000);
                    }
                    else
                    {
                        try{Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
                        catch (FormatException)
                        {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1;}
                        try{ Lat = Convert.ToDouble(TextBoxLeftDown1.Text);}
                        catch (FormatException)
                        { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                            ErrorLabel.Content = rb.Message1; }

                       

                        GeoCalculator.ClassGeoCalculator.f_Grad_GM(Long, ref IntLong2, ref MinLong);
                        GeoCalculator.ClassGeoCalculator.f_Grad_GM(Lat, ref IntLat2, ref MinLat);

                        TextBoxLeftUp1.Text = Convert.ToString(IntLong2);
                        TextBoxLeftDown1.Text = Convert.ToString(IntLat2);

                        TextBoxLeftUp2.Text = Convert.ToString(Math.Round(MinLong, 4));
                        TextBoxLeftDown2.Text = Convert.ToString(Math.Round(MinLat, 4));
                    }
                }
                else if (TypeOfEnter == 4)
                {
                    try{ Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
                    catch (FormatException)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}
                    try{Lat = Convert.ToDouble(TextBoxLeftDown1.Text);}
                    catch (FormatException)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}

                    try{MinLong = Convert.ToDouble(TextBoxLeftUp2.Text);}
                    catch (Exception)
                    {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}
                    try{MinLat = Convert.ToDouble(TextBoxLeftDown2.Text);}
                    catch (Exception)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}

                    try{SekLong = Convert.ToDouble(TextBoxLeftUp3.Text);}
                    catch (Exception)
                    {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}
                    try{ SekLat = Convert.ToDouble(TextBoxLeftDown3.Text);}
                    catch (Exception)
                    {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}

                    GeoCalculator.ClassGeoCalculator.f_GMS_GM((int)Long, (int)MinLong, SekLong, ref IntLong2, ref MinLong);
                    GeoCalculator.ClassGeoCalculator.f_GMS_GM((int)Lat, (int)MinLat, SekLat, ref IntLat2, ref MinLat);

                    TextBoxLeftUp1.Text = Convert.ToString(IntLong2);
                    TextBoxLeftDown1.Text = Convert.ToString(IntLat2);

                    TextBoxLeftUp2.Text = Convert.ToString(Math.Round(MinLong, 4));
                    TextBoxLeftDown2.Text = Convert.ToString(Math.Round(MinLat, 4));
                }
                break;
            }
            TypeOfEnter = 3;
            TextBoxLeftUp3.Text = "";
            TextBoxLeftDown3.Text = "";
            TextBoxLeftUp2.IsReadOnly = false;
            TextBoxLeftDown2.IsReadOnly = false;
            TextBoxLeftUp3.IsReadOnly = true;
            TextBoxLeftDown3.IsReadOnly = true;

            if (SystemOfCoordinate1 == 3)
            {
                Label1.Content = "км.";
                Label2.Content = "км.";
                TextBoxLeftDown2.Visibility = Visibility.Collapsed;
                TextBoxLeftUp2.Visibility = Visibility.Collapsed;
                Label11.Visibility = Visibility.Collapsed;
                Label12.Visibility = Visibility.Collapsed;
            }
            else
            {
                TextBoxLeftUp2.Visibility = Visibility.Visible;
                TextBoxLeftUp3.Visibility = Visibility.Collapsed;
                TextBoxLeftDown2.Visibility = Visibility.Visible;
                TextBoxLeftDown3.Visibility = Visibility.Collapsed;

                Label11.Visibility = Visibility.Visible;
                Label12.Visibility = Visibility.Collapsed;
                Label21.Visibility = Visibility.Visible;
                Label22.Visibility = Visibility.Collapsed;
            }
        }
        private void RadioButton_Checked_14(object sender, RoutedEventArgs e)
        {
            ErrorLabel.Content = "";
            while (true)
            {
                if (TypeOfEnter == 3)
                {
                    try{Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
                    catch (FormatException){
                        ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}
                    try{Lat = Convert.ToDouble(TextBoxLeftDown1.Text);}
                    catch (FormatException)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}

                    try{ MinLong = Convert.ToDouble(TextBoxLeftUp2.Text);}
                    catch (Exception){
                        ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}
                    try {MinLat = Convert.ToDouble(TextBoxLeftDown2.Text);}
                    catch (Exception)
                    { ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}

                    GeoCalculator.ClassGeoCalculator.f_GM_GMS((int)Long, MinLong, ref IntLong2, ref IntMinLong2, ref SekLong);
                    GeoCalculator.ClassGeoCalculator.f_GM_GMS((int)Lat, MinLat, ref IntLat2, ref IntMinLat2, ref SekLat);

                    TextBoxLeftUp1.Text = Convert.ToString(IntLong2);
                    TextBoxLeftDown1.Text = Convert.ToString(IntLat2);

                    TextBoxLeftUp2.Text = Convert.ToString(IntMinLong2);
                    TextBoxLeftDown2.Text = Convert.ToString(IntMinLat2);
                    TextBoxLeftUp3.Text = Convert.ToString(Math.Round(SekLong, 2));
                    TextBoxLeftDown3.Text = Convert.ToString(Math.Round(SekLat, 2));
                }
                else if (TypeOfEnter == 2)
                {
                    try {Long = Convert.ToDouble(TextBoxLeftUp1.Text);}
                    catch (FormatException) {
                        ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1; }
                    try{ Lat = Convert.ToDouble(TextBoxLeftDown1.Text); }
                    catch (FormatException)
                    {ExeptionRB rb = new ExeptionRB(TextBoxLeftUp1.Text, TextBoxLeftDown1.Text);
                        ErrorLabel.Content = rb.Message1;}

                    GeoCalculator.ClassGeoCalculator.f_Grad_GMS(Long, ref IntLong2, ref IntMinLong2, ref SekLong);
                    GeoCalculator.ClassGeoCalculator.f_Grad_GMS(Lat, ref IntLat2, ref IntMinLat2, ref SekLat);

                    TextBoxLeftUp1.Text = Convert.ToString(IntLong2);
                    TextBoxLeftDown1.Text = Convert.ToString(IntLat2);

                    TextBoxLeftUp2.Text = Convert.ToString(IntMinLong2);
                    TextBoxLeftDown2.Text = Convert.ToString(IntMinLat2);
                    TextBoxLeftUp3.Text = Convert.ToString(Math.Round(SekLong, 2));
                    TextBoxLeftDown3.Text = Convert.ToString(Math.Round(SekLat, 2));
                }
                break;
            }

            TypeOfEnter = 4;
            TextBoxLeftUp2.IsReadOnly = false;
            TextBoxLeftUp3.IsReadOnly = false;
            TextBoxLeftDown2.IsReadOnly = false;
            TextBoxLeftDown3.IsReadOnly = false;

            TextBoxLeftUp2.Visibility = Visibility.Visible;
            TextBoxLeftUp3.Visibility = Visibility.Visible;
            TextBoxLeftDown2.Visibility = Visibility.Visible;
            TextBoxLeftDown3.Visibility = Visibility.Visible;

            Label11.Visibility = Visibility.Visible;
            Label12.Visibility = Visibility.Visible;
            Label21.Visibility = Visibility.Visible;
            Label22.Visibility = Visibility.Visible;

        }
    }
}
