﻿
using System.Windows;
using System.Windows.Controls;
using System.Windows.Documents;
using System.Windows.Media;


namespace Geo_Calculator
{
    partial class MainWindow : Window
    {
        private void MenuItem_Click_Save(object sender, RoutedEventArgs e)
        {
            Microsoft.Win32.SaveFileDialog dlg = new Microsoft.Win32.SaveFileDialog();
            dlg.FileName = "Document";
            dlg.Filter = "txt files(*.txt) |*.txt| csv files(*.csv)|*.csv| All files(*.*) |*.* ";
            dlg.FilterIndex = 2;
            if (dlg.ShowDialog() == true)
            {
                using (System.IO.StreamWriter sw = new System.IO.StreamWriter(dlg.OpenFile(), System.Text.Encoding.Default))
                {
                    if (dlg.FilterIndex == 2)
                    {
                        sw.Write("N°" + " , " + inf.Header + " , " + lo.Header + " , " + la.Header + " , " + w.Header + " , " + c.Header + " , " + me.Header + " , " + mg.Header);
                        sw.Write("\r");
                        for (int i = 0; i < Counter; i++)
                        {
                            sw.Write(list2[i].Num + " , " + list2[i].Info.Trim(charsToTrim).Replace("\n", " /  ") + " , " + list2[i].Lat.Trim(charsToTrim).Replace("\n", " /  ") + " , " + list2[i].Long.Trim(charsToTrim).Replace("\n", " /  ") + " , " + list2[i].WGS84.Trim(charsToTrim).Replace("\n", " /  ") + " , " + list2[i].CK42.Trim(charsToTrim).Replace("\n", " /  ") + " , " + list2[i].Merkator.Trim(charsToTrim).Replace("\n", " /  ") + " , " + list2[i].MGRS.Trim(charsToTrim).Replace("\n", " /  "));
                            sw.Write("\r");
                        }
                    }
                    if (dlg.FilterIndex == 1)
                    {
                        sw.Write("N°" + "   " + inf.Header + "   " + lo.Header + "             " + la.Header + "                                     " + w.Header + "                                     " + c.Header + "                                     " + me.Header + "                                     " + mg.Header);
                        sw.Write("\r");
                        for (int i = 0; i < Counter; i++)
                        {
                            sw.Write(list2[i].Num + "   " + list2[i].Info.Trim(charsToTrim).Replace("\n", " /  ") + "   " + list2[i].Lat.Trim(charsToTrim).Replace("\n", " /  ") + "   " + list2[i].Long.Trim(charsToTrim).Replace("\n", " /  ") + "   " + list2[i].WGS84.Trim(charsToTrim).Replace("\n", " /  ") + "   " + list2[i].CK42.Trim(charsToTrim).Replace("\n", " /  ") + "   " + list2[i].Merkator.Trim(charsToTrim).Replace("\n", " /  ") + "   " + list2[i].MGRS.Trim(charsToTrim).Replace("\n", " /  "));
                            sw.Write("\r");
                        }
                    }
                    sw.Close();
                }
            }
        }

        private int pageCount;

        private void Button_ClickPrint(object sender, RoutedEventArgs e)
        {
            

            PrintDialog printDialog = new PrintDialog();
            if (printDialog.ShowDialog() == true)
            {
                // Создать визуальный элемент для страницы

                // Определить текст, который необходимо печатать
                MainClases.StoreDataSetPaginator paginator = new MainClases.StoreDataSetPaginator(list2,
                    new Typeface("Calibri"), 14, 96 * 0.25,
                    new Size(printDialog.PrintableAreaWidth, printDialog.PrintableAreaHeight), Counter);

                    printDialog.PrintDocument(paginator, "Печать с помощью классов визуального уровня");

            }
        
        }
    }
}


